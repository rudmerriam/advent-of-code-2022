/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <algorithm>
#include <forward_list>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <ranges>
#include <set>
#include <variant>
#include <stack>

#include "../AdventCpp.h"

namespace rng = std::ranges;
//---------------------------------------------------------------------------------------------------------------------------
struct Line {
    friend std::istream& operator>>(std::istream& is, Line& line) {
        std::getline(is, line.mLine);
        return is;
    }
    friend std::ostream& operator<<(std::ostream& os, Line const& line) {
        os << line.mLine;
        return os;
    }
    std::string mLine;
};
//---------------------------------------------------------------------------------------------------------------------------
struct Terminal {

    Terminal() = default;
    long operator()(std::istream& data);

    using Dir = std::pair<std::string, long>;

private:
    struct DirSizes {
        int pos;
        long size;
    };
    void doCd();
    void doCommand();
    void doDir(std::string const&);
    [[nodiscard]] long doFile(std::string const&) const;
    long doLs();
    long findDir();
    void loadData(std::istream& data);
    static long sumDirs(std::vector<Dir>& dirs);

//    std::set<Dir> mDirs;
    std::vector<Line> mLines;
    std::vector<Line>::iterator mIt;
    int mDepth { };
    std::stack<Dir> mDirStack;
    std::set<Dir> mDirList;
    long walkDirs(std::vector<Dir> dirs);
    void dirWalk(std::vector<Dir>& dirs, auto dirs_it, auto next_it);
    long addDirs(std::vector<Dir> const& dirs);
};
//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, Terminal::Dir const& d) {
    std::cout << '[' << std::setw(10) << d.second << tab << d.first << ']';
    return os;
}
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
void print_dirs(T const& dirs) {
    for (auto d: dirs) {
        std::cout << d << nl;
    }
}
//---------------------------------------------------------------------------------------------------------------------------
long Terminal::operator()(std::istream& data) {
    loadData(data);
    Dir new_dir {"/", 0};
    mDirStack.emplace(new_dir);

    mDirList.insert({"/", 0});

    doCommand();

    long dir_size {findDir()};

#if 1
    return dir_size;
#else
    return 95437;
#endif
}
//---------------------------------------------------------------------------------------------------------------------------
void Terminal::doCommand() {

    while (mIt < mLines.end()) {
        std::cout << std::setw(10) << __FUNCTION__ << std::setw(1 + (3 * mDepth)) << sp << mIt->mLine << tab
                  << mDirStack.top() << nl;

        switch (mIt->mLine[2]) {
            case 'c':
                doCd();
//                std::cout << std::setw(10) << __FUNCTION__ << std::setw(1 + (3 * mDepth)) << sp << mDirStack.top() << nl;
                break;

            case 'l':
//                std::cout << std::setw(10) << __FUNCTION__ << std::setw(1 + (3 * mDepth)) << sp << mDirStack.top() << nl;
                doLs();
                break;
        }
        mIt++;
    }
}
//---------------------------------------------------------------------------------------------------------------------------
void Terminal::doCd() {

    Dir cur_dir {mDirStack.top()};
    if (cur_dir.first != "/") {
        cur_dir.first += +"/";
    }

    std::cout << std::setw(10) << __FUNCTION__ << "  " << std::setw(1 + (3 * mDepth)) << sp << mIt->mLine << tab << cur_dir
              << nl;

    if (mIt->mLine.substr(5) == "..") {
        mDirStack.pop();

    } else if (mIt->mLine.substr(5) == "/") {
        static Dir slash_dir {"/", 0};
        mDirStack = { };
        mDirStack.emplace(slash_dir);
        mDepth = 0;
    } else {
        Dir new_dir {cur_dir.first + mIt->mLine.substr(5), 0};
        mDirStack.emplace(new_dir);
        mDepth++;

    }
}
//---------------------------------------------------------------------------------------------------------------------------
long Terminal::doLs() {
    Dir cur_dir {mDirStack.top()};

    std::cout << std::setw(10) << __FUNCTION__ << std::setw(1 + (3 * mDepth)) << sp << mIt->mLine << tab << cur_dir << nl;
    mIt++;

    long files_size { };
    while ((mIt != mLines.end()) && (mIt->mLine[0] != '$')) {
//        std::cout << std::setw(10) << __FUNCTION__ << std::setw(1+(3 * mDepth)) << sp << mIt->mLine << nl;

        if (mIt->mLine[0] == 'd') {
            doDir(mIt->mLine);
        } else {
            files_size += doFile(mIt->mLine);
        }
        mIt++;
    }

    doCommand();

    mDirList.erase({cur_dir});
    cur_dir.second = files_size;
    mDirList.emplace(cur_dir);

    return files_size;
}
//---------------------------------------------------------------------------------------------------------------------------
void Terminal::doDir(std::string const& line) {
    Dir cur_dir {mDirStack.top()};
    std::cout << std::setw(10) << __FUNCTION__ << std::setw(1 + (3 * mDepth)) << sp << line << tab << cur_dir << nl;
    if (cur_dir.first != "/") {
        cur_dir.first += +"/";
    }

    std::string dir {line.substr(4)};
    Dir new_dir {cur_dir.first + dir, 0};
    [[maybe_unused]] auto [iter, did_insert] {mDirList.insert(new_dir)};
}
//---------------------------------------------------------------------------------------------------------------------------
long Terminal::doFile(std::string const& line) const {
    std::cout << std::setw(10) << __FUNCTION__ << std::setw(1 + (3 * mDepth)) << sp << line << nl;
    return std::stol(line);
}
//---------------------------------------------------------------------------------------------------------------------------
void Terminal::loadData(std::istream& data) {
    rng::copy(std::ranges::istream_view<Line>(data), std::back_inserter(mLines));
    mIt = mLines.begin();
}
//---------------------------------------------------------------------------------------------------------------------------
long Terminal::findDir() {

    std::vector<Dir> dir_vec {mDirList.begin(), mDirList.end()};
    std::cout << nl << nl;
    print_dirs(dir_vec);

    std::cout << nl << nl;
    return walkDirs(dir_vec);
}
//---------------------------------------------------------------------------------------------------------------------------
long Terminal::walkDirs(std::vector<Dir> dirs) {
    rng::reverse(dirs);

    dirWalk(dirs, dirs.begin(), dirs.begin() + 1);
    rng::reverse(dirs);

    std::cout << nl << nl;
    print_dirs(dirs);

    std::cout << nl << nl;
    return addDirs(dirs);
}
//---------------------------------------------------------------------------------------------------------------------------
void Terminal::dirWalk(std::vector<Dir>& dirs, auto dirs_it, auto next_it) {
    Dir& cur_dir {*dirs_it};

    auto is_dirs_end {next_it == dirs.end()};

    if (is_dirs_end) {
        std::cout << "exit" << nl;
        std::cout << nl << nl;
        print_dirs(dirs);
        return;
    }

    Dir& next_dir {*next_it};

    if (cur_dir.first == next_dir.first) {

        std::cout << "equal " << std::setw(mDepth * 3) << tab << cur_dir << tab << next_dir << nl;
        dirWalk(dirs, dirs_it, ++next_it);

    } else if (cur_dir.first.starts_with(next_dir.first)) {

        std::cout << "starts" << std::setw(mDepth * 3) << tab << cur_dir << tab << next_dir << nl;
        next_dir.second += cur_dir.second;
        std::cout << "added " << std::setw(mDepth * 3) << tab << cur_dir << tab << next_dir << nl << nl;
        dirWalk(dirs, dirs_it + 1, dirs_it + 2);

    } else {
        std::cout << "NE    " << std::setw(mDepth * 3) << tab << cur_dir << tab << next_dir << nl;
        dirWalk(dirs, dirs_it, ++next_it);
    }
}
//---------------------------------------------------------------------------------------------------------------------------
long Terminal::addDirs(std::vector<Dir> const& dirs) {
    long total { };
    std::cout << nl << nl;

    for (const auto& d: dirs) {
        std::cout << total << tab << d << nl;
        if (d.second < 100000) {
            total += d.second;
            std::cout << "total 1: " << total << tab << d << nl;
        }
//        std::cout << "total 2: " << total << tab << d << nl;
    }
    return total;
}

