/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include "../AdventCpp.h"

//  485
//---------------------------------------------------------------------------------------------------------------------------
execFunc execute = [ ](std::istream& data) {
    int16_t overlap_pairs { };

    int16_t left_start;
    int16_t left_end;
    int16_t right_start;
    int16_t right_end;

    char throw_away;
    while (data.good()) {
        data >> left_start >> throw_away >> left_end >> throw_away >> right_start >> throw_away >> right_end;

        std::cout << left_start << ' ' << left_end << ' ' << right_start << ' ' << right_end << '\n';

        data.get();     // last new line
        data.peek();    // trigger eof so file isn't 'good'

        // check for overlapping sections


        bool is_not_overlapped = ((left_start >= right_start) && (left_end <= right_end)) ||
                                 ((right_start >= left_start) && (right_end <= left_end));

        if (is_not_overlapped) {
            ++overlap_pairs;
            std::cout << "Overlap: " << left_start << ' ' << left_end << ' ' << right_start << ' ' << right_end << '\n';
        }

    }
    std::cout << "Total overlaps: " << overlap_pairs << '\n';
    return overlap_pairs;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day04/data_4.txt"};

        char const *test_data {"2-4,6-8\n"
                               "2-3, 4-5\n"
                               "5-7, 7-9\n"
                               "2-8, 3-7\n"
                               "3-7, 2-8\n"
                               "6-6, 4-6\n"
                               "4-6, 6-6\n"
                               "2-6, 4-8\n"
                               "4-8, 2-6\n"};
        AdventCpp run(data_file, execute, 4, test_data);

        run.validate();
        auto res {run.execute()};
        std::cout << "Final result: " << res << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
