/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#pragma once

#define USE_THREE_WAY
//---------------------------------------------------------------------------------------------------------------------------
struct PairPosition {
    PairPosition() = default;
    PairPosition(int x, int y) : mPair {x, y} {
    }
    PairPosition(PairPosition const&) = default;

    int& x = mPair.first;
    int& y = mPair.second;
    std::pair<int, int> mPair { };

    PairPosition operator+=(PairPosition const rhs);
    PairPosition operator-=(PairPosition const rhs);
    PairPosition& operator=(PairPosition const&);

#ifdef USE_THREE_WAY \
//    std::strong_ordering operator<=>(const PairPosition&) const = default;
//    bool operator==(PairPosition const& rhs) const = default;

    std::strong_ordering operator<=>(const PairPosition) const;
    bool operator==(PairPosition const rhs) const;
#else
    // auto generated with CLion
    bool operator==(PairPosition const& rhs) const;
    bool operator<(PairPosition const& rhs) const;
    bool operator>(PairPosition const& rhs) const;
    bool operator<=(PairPosition const& rhs) const;
    bool operator>=(PairPosition const& rhs) const;
#endif

    friend PairPosition abs(PairPosition const rhs);
    friend std::ostream& operator<<(std::ostream& os, PairPosition const p);
};
//---------------------------------------------------------------------------------------------------------------------------
PairPosition PairPosition::operator+=(PairPosition const rhs) {
    return {x += rhs.x, y += rhs.y};
}
//---------------------------------------------------------------------------------------------------------------------------
PairPosition PairPosition::operator-=(PairPosition const rhs) {
    return {x -= rhs.x, y -= rhs.y};
}
//---------------------------------------------------------------------------------------------------------------------------
PairPosition& PairPosition::operator=(PairPosition const& rhs) {
    if (this != &rhs) {
        x = rhs.x;
        y = rhs.y;
    }
    return *this;
}
//---------------------------------------------------------------------------------------------------------------------------
PairPosition operator+(PairPosition const lhs, PairPosition const rhs) {
    PairPosition res {lhs};
    return res += rhs;
}
//---------------------------------------------------------------------------------------------------------------------------
PairPosition operator-(PairPosition const lhs, PairPosition const rhs) {
    PairPosition res {lhs};
    res = (res -= rhs); // weird - doesn't work without this
    return (res -= rhs);
}
//---------------------------------------------------------------------------------------------------------------------------
bool PairPosition::operator==(PairPosition const rhs) const {
    return x == rhs.x && y == rhs.y;
}
//---------------------------------------------------------------------------------------------------------------------------
PairPosition abs(PairPosition const rhs) {
    PairPosition res;
    res.mPair = {::abs(rhs.x), ::abs(rhs.y)};
    return res;
}
//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, PairPosition const p) {
    os << p.x << ' ' << p.y;
    return os;
}
//---------------------------------------------------------------------------------------------------------------------------
#ifdef USE_THREE_WAY
std::strong_ordering PairPosition::operator<=>(PairPosition const rhs) const {
    return mPair <=> rhs.mPair;
}
#else
//---------------------------------------------------------------------------------------------------------------------------
bool PairPosition::operator<(PairPosition const& rhs) const {
    return mPair < rhs.mPair;
}
bool PairPosition::operator>(PairPosition const& rhs) const {
    return rhs < *this;
}
bool PairPosition::operator<=(PairPosition const& rhs) const {
    return !(rhs < *this);
}
bool PairPosition::operator>=(PairPosition const& rhs) const {
    return !(*this < rhs);
}

#endif
