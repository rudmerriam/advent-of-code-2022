/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include "../AdventCpp.h"
#include "Terminal_7_1.h"

//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day07/data_7.txt"};
        char const *test_data {"$ cd /\n"
                               "$ cd /\n"
                               "$ ls\n"
                               "dir a\n"
                               "14848514 b.txt\n"
                               "8504156 c.dat\n"
                               "dir d\n"
                               "$ cd a\n"
                               "$ ls\n"
                               "dir e\n"
                               "29116 f\n"
                               "2557 g\n"
                               "62596 h.lst\n"
                               "$ cd e\n"
                               "$ ls\n"
                               "584 i\n"
                               "$ cd ..\n"
                               "$ cd ..\n"
                               "$ cd d\n"
                               "$ ls\n"
                               "4060174 j\n"
                               "8033020 d.log\n"
                               "5626152 d.ext\n"
                               "7214296 k\n"
// 23352670
        };

        std::cout << std::boolalpha;
        Terminal terminal;
        AdventCpp run(data_file, terminal, 95437, test_data);

        run.validate();
        auto res {run.execute()};   // 1118405
        std::cout << "Final result: " << res << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
