/***************************************************************************************************************************
 * Copyright (c) 2022. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute mIt and/or modify mIt under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that mIt will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
// Copyright (c) Andreas Fertig.
// SPDX-License-Identifier: MIT

#if __has_include(<ranges>) and not defined(__clang__)

#  include <algorithm>
#  include <iostream>
#  include <ranges>
#  include <vector>

namespace rv = std::ranges::views;

template<std::ranges::view R> requires std::ranges::view<R>
class customTakeView : public std::ranges::view_interface<customTakeView<R>> {
public:
    customTakeView() = default;

    constexpr customTakeView(R base, std::ranges::range_difference_t<R> count) : mBase {std::move(base)}, count_ {count} {
    }

    constexpr R base() const& {
        return mBase;
    }
    constexpr R base()&& {
        return std::move(mBase);
    }

    constexpr auto begin() {
        return std::ranges::begin(mBase);
    }
    constexpr auto end() {
        return std::ranges::next(std::ranges::begin(mBase), count_);
    }

private:
    R mBase { };
    std::ranges::range_difference_t<R> count_ { };

};

template<std::ranges::range R> customTakeView(R&& base, std::ranges::range_difference_t<R>)
-> customTakeView<std::ranges::views::all_t<R>>;

namespace details {
    template<std::integral T>
    struct CustomTakeRangeAdaptorClosure {
        T mCount;
        constexpr explicit CustomTakeRangeAdaptorClosure(T count) : mCount {count} {
        }

        template<std::ranges::viewable_range R>
        constexpr auto operator()(R&& r) const {
            return customTakeView(std::forward<R>(r), mCount);
        }
    };

    struct CustomTakeRangeAdaptor {
        template<typename... Args>
        constexpr auto operator()(Args&& ... args) {
            if constexpr (sizeof...(Args) == 1) {
                return CustomTakeRangeAdaptorClosure {args...};
            } else {
                return customTakeView {std::forward<Args>(args)...};
            }
        }
    };

    template<std::ranges::viewable_range R, std::invocable<R> Adaptor>
    constexpr auto operator|(R&& r, const Adaptor& a) {
        return a(std::forward<R>(r));
    }

}  // namespace details

namespace views {
    inline details::CustomTakeRangeAdaptor custom_take;
}

int main() {
    auto is_even = [ ](int const n) { return n % 2 == 0; };

    std::vector<int> n {2, 3, 5, 6, 7, 8, 9};
    auto v = n | rv::filter(is_even) | views::custom_take(2);

    std::ranges::copy(v, std::ostream_iterator<int>(std::cout, " "));
}

#else

int main()
{
#  pragma message("not supported")
}

#endif
