/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

//
// Created by rmerriam on 12/22/22.
//

#pragma once

#include <ranges>
#include <set>
#include <unordered_set>

//---------------------------------------------------------------------------------------------------------------------------
class Mover {

public:
    Mover() {
    };
    int operator()(std::istream& data);
    //-----------------------------------------------------------------------------------------------------------------------
    struct Move {
        char mDirection;
        int mSteps;
        friend std::istream& operator>>(std::istream& is, Move& m);
        friend std::ostream& operator<<(std::ostream& os, Move m);
    };
    //-----------------------------------------------------------------------------------------------------------------------
    struct Position {
        int x { };
        int y { };

        bool operator<(Position const rhs) const {
            return x < rhs.x || y < rhs.y;
        }
        Position& operator-=(Position const& rhs);
        friend std::ostream& operator<<(std::ostream& os, Mover::Position p);
    };
private:
    void moveHead(Move const& m);
    void moveTail();

    Position mTail;
    Position mHead;
    std::set<Position> mTrail {{0, 0}};
};
//---------------------------------------------------------------------------------------------------------------------------
std::istream& operator>>(std::istream& is, Mover::Move& m) {
    is >> m.mDirection >> m.mSteps;
    return is;
}
//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, Mover::Move m) {
    os << m.mDirection << sp << m.mSteps;
    return os;
}
//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, Mover::Position p) {
    os << p.x << sp << p.y;
    return os;
}
//---------------------------------------------------------------------------------------------------------------------------
Mover::Position& Mover::Position::operator-=(Mover::Position const& rhs) {
    x -= rhs.x;
    y -= rhs.y;
    return *this;
}
//---------------------------------------------------------------------------------------------------------------------------
Mover::Position operator-(Mover::Position const& lhs, Mover::Position const& rhs) {
    return Mover::Position {lhs} -= rhs;
}
//---------------------------------------------------------------------------------------------------------------------------
int32_t Mover::operator()(std::istream& data) {
    std::vector<Move> moves;

    std::ranges::copy(std::ranges::istream_view<Move>(data), std::back_inserter(moves));
    print(moves, " \n");
    std::cout << nl;

    mTrail.insert(mTail);

    for (auto const& m: moves) {
        std::cout << m;
        std::cout << tab << tab << mHead << sp << tab << mTail << nl;

        for (int16_t i { }; i < std::abs(m.mSteps); ++i) {

            moveHead(m);
            moveTail();
            mTrail.insert(mTail);

            std::cout << tab << tab << mHead << sp << tab << mTail << nl;
        }
        std::cout << "--------------" << nl;
    }
    nlf();

    std::cout << "Trail\n";
    print(mTrail, " \n");
    nlf();
    return mTrail.size();
}
//---------------------------------------------------------------------------------------------------------------------------
void Mover::moveHead(Mover::Move const& m) {
    switch (m.mDirection) {
        case 'U':
            ++mHead.y;
            break;
        case 'D':
            --mHead.y;
            break;

        case 'R':
            ++mHead.x;
            break;
        case 'L':
            --mHead.x;
            break;
    }
}
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
auto sign(T a) -> T {
    return T(a > 0 ? 1 : -1);
}
//---------------------------------------------------------------------------------------------------------------------------
void Mover::moveTail() {
    if (std::abs(mHead.y - mTail.y) >= 2) {
        mTail.y += (mHead.y - mTail.y) / 2;

        if (std::abs(mHead.x - mTail.x) >= 2) {
            mTail.x += (mHead.x - mTail.x) / 2;
        } else {
            mTail.x += mHead.x - mTail.x;
        }

    } else if (std::abs(mHead.x - mTail.x) >= 2) {
        mTail.x += (mHead.x - mTail.x) / 2;
        mTail.y += mHead.y - mTail.y;
    }
}
