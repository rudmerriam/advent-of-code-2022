/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include "../AdventCpp.h"

//---------------------------------------------------------------------------------------------------------------------------
execFunc execute = [ ](std::istream& data) {
    int16_t points_sum { };

    while (data.good()) {
        std::string line;
        std::getline(data, line);
        auto mid_point {line.size() / 2};
        std::cout << line.substr(0, mid_point) << " | " << line.substr(mid_point + 1) << '\n';

        std::string_view first_partition {line.begin(), line.begin() + mid_point};
        std::string_view second_partition {line.begin() + mid_point + 1, line.end()};

        auto loc {first_partition.find_first_of(second_partition)};
        auto ch {first_partition[loc]};

        std::cout << ch;

        if (ch >= 'a') {
            ch -= 'a' - 1;
        } else {
            ch -= 'A' - 1;
            ch += 26;
        }
        points_sum += ch;
        std::cout << ' ' << int16_t(ch) << ' ' << points_sum << '\n';
        data.peek();
    }
    return points_sum;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day03/data_3.txt"};
        char const *test_data {"vJrwpWtwJgWrhcsFMMfFFhFp\n"
                               "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\n"
                               "PmmdzqPrVvPwwTWBwg\n"
                               "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\n"
                               "ttgJtRGJQctTZtZT\n"
                               "CrZsJsPPZsGzwwsLwLmpwMDw\n"};

        AdventCpp run(data_file, execute, 157, test_data);

        run.validate();
        auto res {run.execute()};
        std::cout << "Final result: " << res << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
