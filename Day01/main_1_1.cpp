/***************************************************************************************************************************
 * Copyright (c) 2023. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#include <iostream>
#include <fstream>

//---------------------------------------------------------------------------------------------------------------------------
int64_t get_calories(std::ifstream& data) {
    int64_t calories;
    data >> calories;
    data.get();     // eat nl
    std::cout << calories << '\t';
    return calories;
}
//---------------------------------------------------------------------------------------------------------------------------
void open_data(std::ifstream& data) {
    if (!data.is_open()) {
        std::cout << "file not open" << '\n';
        exit(-2);
    }
}
//---------------------------------------------------------------------------------------------------------------------------
// 67622
int main() {

    std::ifstream data("../Day01/data_1.txt");
    open_data(data);

    data >> std::noskipws;

    int64_t max_calories { };

    while (data.good()) {
        static int64_t elf_calories { };

        elf_calories += get_calories(data);
        std::cout << elf_calories << '\n';

        if (data.peek() == '\n' || data.eof()) {
            data.get();     // eat nl
            std::cout << '\n';

            max_calories = std::max(max_calories, elf_calories);
            std::cout << elf_calories << '\t' << max_calories << "\n\n";
            elf_calories = 0;
        }
    }

    return 0;
}
