/***************************************************************************************************************************
 * Copyright (c) 2022. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include "AdventCpp.h"

//---------------------------------------------------------------------------------------------------------------------------
ExecFunc execute = [ ](std::istream& data) {
    int16_t result { };

    return result;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {""};

        char const *test_data {""};
        AdventCpp run(data_file, execute, 4, test_data);

        run.validate();
        auto res {run.execute()};
        std::cout << "Final result: " << res << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
