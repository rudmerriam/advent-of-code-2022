/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include "../AdventCpp.h"

//---------------------------------------------------------------------------------------------------------------------------
using TreeRow = std::vector<uint8_t>;
using Forest = std::vector<TreeRow>;;
//---------------------------------------------------------------------------------------------------------------------------
void dump_forest(Forest forest) {
    std::cout << '\n';
    for (auto& row: forest) {
        for (auto col: row) {
            std::cout << col;
        }
        std::cout << '\n';
    }
    std::cout << '\n';
}
//---------------------------------------------------------------------------------------------------------------------------
void build_forest(std::istream& data, Forest& forest) {
    data >> std::noskipws;

    while (data.good()) {
        char ch;
        data >> ch;

        if (ch == '\n') {
            forest.emplace_back();
        } else {
            forest.back().emplace_back(ch);
        }

        data.peek();
    }
    dump_forest(forest);
}
//---------------------------------------------------------------------------------------------------------------------------
int32_t perimeter_cnt(Forest& forest) {
    int16_t const rows {(int16_t) forest.size()};
    int16_t const cols {(int16_t) forest.front().size()};
    int32_t const perimeter_cnt {(cols * 2) + (rows - 2) * 2};

    std::cout << "Forest size: " << cols << " by " << rows << '\n';
    return perimeter_cnt;
}
//---------------------------------------------------------------------------------------------------------------------------
execFunc execute = [ ](std::istream& data) {
    int16_t result { };

    Forest forest;
    forest.emplace_back();

    build_forest(data, forest);
    forest.pop_back();  // empty row added by final nl

    int32_t const p_cnt {perimeter_cnt(forest)};
    std::cout << "Perimeter: " << p_cnt << '\n';

    return result;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day08/data_8.txt"};

        char const *test_data {"30373\n"
                               "25512\n"
                               "65332\n"
                               "33549\n"
                               "35390\n"};
        AdventCpp run(data_file, execute, 21, test_data);

        run.validate();
        auto res {run.execute()};
        std::cout << "Final result: " << res << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
