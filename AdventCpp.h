/***************************************************************************************************************************
 * Copyright (c) 2022. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute mIt and/or modify mIt under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that mIt will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

//
// Created by rmerriam on 12/17/22.
//

#pragma once

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>

//---------------------------------------------------------------------------------------------------------------------------
const char nl {'\n'};
const char sp {' '};
const char tab {'\t'};
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
void print(T& t, char const *delimiter = "") {
    std::ranges::copy(t, std::ostream_iterator<typename T::value_type>(std::cout, delimiter));
}
//---------------------------------------------------------------------------------------------------------------------------
void nlf() {
    std::cout << '\n';
}
//---------------------------------------------------------------------------------------------------------------------------
// function to do the processing of the advent data. It is passed as constructor parameter.

using execFunc = std::function<int32_t(std::istream&)>;
//---------------------------------------------------------------------------------------------------------------------------
struct AdventCpp {
    AdventCpp(char const *file_name, execFunc exec_func, int32_t const expected_value, char const *test_data);

    int32_t validate();
    int32_t execute() {
        return mExecFunc(mDataFile);
    }
private:
    void openData(char const *file_name);

    std::ifstream mDataFile;
    execFunc mExecFunc;
    int32_t mExpectedValue;
    char const *mTestData;
};
//---------------------------------------------------------------------------------------------------------------------------
inline AdventCpp::AdventCpp(char const *file_name, execFunc exec_func, int32_t const expected_value, char const *test_data)
    : mExecFunc {std::move(exec_func)}, mExpectedValue {expected_value}, mTestData {test_data} {
    openData(file_name);
}
//---------------------------------------------------------------------------------------------------------------------------
inline void AdventCpp::openData(char const *file_name) {
    mDataFile.open(file_name);
    if (!mDataFile.is_open()) {
        throw std::runtime_error(std::string("Cannot open data file: ") + file_name);
    }
}
//---------------------------------------------------------------------------------------------------------------------------
inline int32_t AdventCpp::validate() {
    std::istringstream test_file(mTestData);

    std::cout << "===============================\n"
                 "Validating with example data. Data size is:" << test_file.view().size() << "\n\n";

    int32_t res {mExecFunc(test_file)};

    if (res == mExpectedValue) {
        std::cout << "\n\nValidation completed successfully\n"
                     "===============================\n";
    } else {
        std::string what {
            "\n\nValidation failed\nExpected " + std::to_string(mExpectedValue) + " but calculated " + std::to_string(res) +
            "\n===============================\n"};
        throw std::logic_error {what};
    }

    return res;
}
