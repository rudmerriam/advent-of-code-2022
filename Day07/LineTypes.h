/***************************************************************************************************************************
 * Copyright (c) 2023. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

//
// Created by rmerriam on 1/1/23.
//

#pragma once

#include <utility>
#include <variant>
#include <string>

//---------------------------------------------------------------------------------------------------------------------------
struct LineTypesBase {
    [[maybe_unused]] explicit LineTypesBase(std::string const&& name) : mName {name} {
    }
    std::string mName;
};
//---------------------------------------------------------------------------------------------------------------------------
struct ChangeDir : public LineTypesBase {
    explicit ChangeDir() : LineTypesBase {__func__} {
    }
};
//---------------------------------------------------------------------------------------------------------------------------
struct ChangeDirHome : public LineTypesBase {
    explicit ChangeDirHome() : LineTypesBase {__func__} {
    }
};
//---------------------------------------------------------------------------------------------------------------------------
struct ChangeDirUp : public LineTypesBase {    // special case for CD ..
    explicit ChangeDirUp() : LineTypesBase {__func__} {
    }
};
//---------------------------------------------------------------------------------------------------------------------------
struct ListFiles : public LineTypesBase {
    explicit ListFiles() : LineTypesBase {__func__} {
    }
};
//---------------------------------------------------------------------------------------------------------------------------
struct SubDirName : public LineTypesBase {
    explicit SubDirName() : LineTypesBase {__func__} {
    }
};
//---------------------------------------------------------------------------------------------------------------------------
struct FileSize : public LineTypesBase {
    explicit FileSize() : LineTypesBase {__func__} {
    }
};

using linesVariant = std::variant<ChangeDir, ChangeDirHome, ChangeDirUp, ListFiles, SubDirName, FileSize>;
