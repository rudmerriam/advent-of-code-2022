/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#pragma once

#include <array>
#include <algorithm>

namespace rng = std::ranges;
//---------------------------------------------------------------------------------------------------------------------------
#define USE_THREE_WAY
//---------------------------------------------------------------------------------------------------------------------------
struct ArrayPosition : public std::array<int, 2> {
    ArrayPosition() : std::array<int, 2> { } {
    };
    ArrayPosition(int x, int y) : std::array<int, 2> {x, y} {
    }
    ArrayPosition(ArrayPosition const&) = default;

    int& x = at(0);
    int& y = at(1);

    ArrayPosition operator+=(ArrayPosition const rhs);
    ArrayPosition operator-=(ArrayPosition const rhs);
    ArrayPosition& operator=(ArrayPosition const& rhs);

//    friend ArrayPosition abs(ArrayPosition const& rhs);
};
//---------------------------------------------------------------------------------------------------------------------------
ArrayPosition ArrayPosition::operator+=(ArrayPosition const rhs) {
    rng::transform(*this, rhs, this->begin(), std::plus { });
    return *this;
}
//---------------------------------------------------------------------------------------------------------------------------
ArrayPosition ArrayPosition::operator-=(ArrayPosition const rhs) {
    rng::transform(*this, rhs, this->begin(), std::minus { });
    return *this;
}
//---------------------------------------------------------------------------------------------------------------------------
ArrayPosition& ArrayPosition::operator=(ArrayPosition const& rhs) {
    if (this != &rhs) {
        x = rhs.x;
        y = rhs.y;
    }
    return *this;
}
//---------------------------------------------------------------------------------------------------------------------------
inline ArrayPosition operator+(ArrayPosition const& lhs, ArrayPosition const& rhs) {
    ArrayPosition ap {lhs};
    ap += rhs;
    return ap;
}
//---------------------------------------------------------------------------------------------------------------------------
inline ArrayPosition operator-(ArrayPosition const& lhs, ArrayPosition const& rhs) {
    ArrayPosition ap {lhs};
    ap -= rhs;
    return ap;
}
//---------------------------------------------------------------------------------------------------------------------------
inline ArrayPosition abs(ArrayPosition const& lhs) {
    ArrayPosition ap;
    rng::transform(lhs, begin(ap), [ ](auto value) {
        return std::abs(value);
    });
    return ap;
}
//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, ArrayPosition const p) {
    os << p.x << ' ' << p.y;
    return os;
}
