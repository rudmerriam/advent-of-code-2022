/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include "../AdventCpp.h"

//---------------------------------------------------------------------------------------------------------------------------
execFunc execute = [ ](std::istream& data) {
    int16_t points_sum { };

    while (data.good()) {
        std::string line1;
        std::getline(data, line1);
        std::cout << line1 << '\n';

        std::string line2;
        std::getline(data, line2);
        std::cout << line2 << '\n';

        std::string_view l1 {line1};
        std::string_view l2 {line2};

        std::string found_chars { };

        for (auto it {l1.begin()}; it != l1.end(); ++it) {
            it = std::ranges::find_first_of(it, l1.end(), l2.begin(), l2.end());

            if (it == l1.end()) {
                break;
            }
            found_chars += *it;
        }
        std::cout << '\t' << found_chars << '\n';

        std::string line3;
        std::getline(data, line3);
        std::cout << line3 << '\n';

        auto loc {found_chars.find_first_of(line3)};
        char ch = found_chars[loc];

        std::cout << ch << '\n';

        if (ch >= 'a') {
            ch += -('a' - 1);
        } else {
            ch += -('A' - 1) + 26;
        }
        points_sum += ch;
        std::cout << ' ' << int16_t(ch) << ' ' << points_sum << '\n';

        data.peek();
    }
    return points_sum;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day03/data_3.txt"};

        char const *test_data {"vJrwpWtwJgWrhcsFMMfFFhFp\n"
                               "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\n"
                               "PmmdzqPrVvPwwTWBwg\n"
                               "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\n"
                               "ttgJtRGJQctTZtZT\n"
                               "CrZsJsPPZsGzwwsLwLmpwMDw\n"};
        AdventCpp run(data_file, execute, 70, test_data);

        run.validate();
        auto res {run.execute()};
        std::cout << "Final result: " << res << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
