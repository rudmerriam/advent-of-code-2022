/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#pragma once

#include <ranges>

#include "Line.h"
#include "LineTypes.h"

#include "../AdventCpp.h"
#include "FileSystem.h"

namespace rng = std::ranges;
namespace view = std::ranges::views;
//---------------------------------------------------------------------------------------------------------------------------
template<class F>
struct Apply {
    F mFn;

    friend void operator|(auto&& r, Apply my) {
        for (auto o: r) {
            my.mFn(o);
        }
    }
};
//---------------------------------------------------------------------------------------------------------------------------
struct ExecView {
    friend void operator|(auto&& view, ExecView) {
        for (auto v: view) { } // execute view
    }
};
//---------------------------------------------------------------------------------------------------------------------------
auto output(Line& line) {
    std::cout << line << nl;
    return 0;
}
//---------------------------------------------------------------------------------------------------------------------------
int terminal(std::istream& in_data) {
    using dir = std::string;
    std::vector<dir> dirs;

    std::istream_iterator<Line> data_it {in_data};
    FileSystem fs;

    view::istream<Line>(in_data) | view::transform(fs) | ExecView();

    return 0;
}
