/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include "../AdventCpp.h"

#include "Mover_5_1.h"

//---------------------------------------------------------------------------------------------------------------------------
execFunc execute = [ ](std::istream& data) {
    int16_t result { };

    Mover mover;
    mover.parseStacks(data);
    mover.parseMoves(data);

    mover.moveCrates();

    mover.dumpStacks();

    std::cout << "Result: " << result << '\n';
    return result;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day05/data_5.txt"};

        char const *test_data {"    [D]    \n"
                               "[N] [C]    \n"
                               "[Z] [M] [P]\n"
                               " 1   2   3 \n"
                               "\n"
                               "move 1 from 2 to 1\n"
                               "move 3 from 1 to 3\n"
                               "move 2 from 2 to 1\n"
                               "move 1 from 1 to 2\n"};
        AdventCpp run(data_file, execute, 0, test_data);

//        run.validate();
        auto res {run.execute()};
        std::cout << "Final result: " << res << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
