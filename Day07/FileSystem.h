/***************************************************************************************************************************
 * Copyright (c) 2023. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

//
// Created by rmerriam on 1/2/23.
//

#pragma once

#include <iostream>
#include <stack>
#include <iomanip>
#include <memory>
#include <map>

#include "LineTypes.h"
#include "Line.h"
#include "../AdventCpp.h"

//---------------------------------------------------------------------------------------------------------------------------
struct FileSystem {
    struct Directory;

    using DirName = std::string;
    using DirSubDirs = std::map<DirName, Directory>;
    using DirPtr = DirSubDirs::iterator;
    using DirStack = std::vector<DirPtr>;

    struct Directory {
        DirName mName { };
        DirSubDirs mSubDirs { };
        long mFileSize { };
    };

    auto operator()(Line l);
    [[nodiscard]] DirSubDirs const& root() const;
    //---------------------------------------------------------------------------------------------------------------------------
    template<class... Ts>
    struct Overloaded : Ts ... {
        using Ts::operator()...;
    };

    // explicit deduction guide (not needed as of C++20)
    template<class... Ts> Overloaded(Ts...) -> Overloaded<Ts...>;
    //---------------------------------------------------------------------------------------------------------------------------

private:
    void changeDir(Line const& l);
    void changeDirUp(Line const& line);
    void listFiles(Line const& line);
    void dirName(Line const& line);
    void fileSize(Line const& line);
    void changeDirHome(Line const& line);

    DirPtr stackBackDir() const;
    DirSubDirs& stackSubDirs();
    DirName const& stackDirName() const;

    DirSubDirs mRoot {{"/", {"/"}}};
    DirStack mDirStack {mRoot.begin()};
    long& stackFileSize() const;
};
//---------------------------------------------------------------------------------------------------------------------------
constexpr int func_width {14};
constexpr int line_width {20};

//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, FileSystem::DirSubDirs const& dir) {
    static int depth {0};

    for (auto& subs: dir) {
        depth++;
        os << std::setw(depth * 1) << sp << subs.first << nl;
        if (subs.second.mSubDirs.size() > 0) {
            os << subs.second.mSubDirs;
        }
        depth--;
    }
    return os;
}
//---------------------------------------------------------------------------------------------------------------------------
auto FileSystem::operator()(Line l) {

    auto handle_lines = Overloaded { //
        [this, &l](ChangeDir const&) { changeDir(l); },  //
        [this, &l](ChangeDirHome const&) { changeDirHome(l); },  //
        [this, &l](ChangeDirUp const&) { changeDirUp(l); }, //
        [this, &l](ListFiles const&) { listFiles(l); }, //
        [this, &l](SubDirName const&) { dirName(l); }, //
        [this, &l](FileSize const&) { fileSize(l); }, //
    };

    std::visit([&handle_lines](auto&& arg) { handle_lines(arg); }, l.mLinesType);
    return l;
}
//---------------------------------------------------------------------------------------------------------------------------
FileSystem::DirName const& FileSystem::stackDirName() const {
    return stackBackDir()->first;
}
//---------------------------------------------------------------------------------------------------------------------------
FileSystem::DirPtr FileSystem::stackBackDir() const {
    return mDirStack.back();
}
//---------------------------------------------------------------------------------------------------------------------------
long& FileSystem::stackFileSize() const {
    auto dir {stackBackDir()};
    return dir->second.mFileSize;
}
//---------------------------------------------------------------------------------------------------------------------------
FileSystem::DirSubDirs& FileSystem::stackSubDirs() {
    return stackBackDir()->second.mSubDirs;
}
//---------------------------------------------------------------------------------------------------------------------------
void FileSystem::fileSize(Line const& line) {
    std::string const& l {line.line()};
    auto file_size = stol(l);

    stackFileSize() += file_size;

    std::cout << std::setw(func_width) << __func__ << tab << '(' << stackDirName() << ')' << tab;
    std::cout << "Size: " << stackFileSize();
    std::cout << sp << std::setw(line_width) << l << nl;
}
//---------------------------------------------------------------------------------------------------------------------------
void FileSystem::listFiles(Line const& line) {
    std::string const& l {line.line()};

    std::cout << std::setw(func_width) << __func__ << tab << '(' << stackDirName() << ')' << tab;
    std::cout << tab << std::setw(line_width) << l << nl;
}
//---------------------------------------------------------------------------------------------------------------------------
void FileSystem::dirName(Line const& line) {
    std::string const& l {line.line()};
    std::cout << std::setw(func_width) << __func__ << tab << '(' << stackDirName() << ')' << tab;

    std::string new_name {l.substr(4)};
    auto& sub_dirs {stackSubDirs()};

    sub_dirs.emplace(std::make_pair(new_name, Directory {new_name}));

    std::cout << "New: " << new_name;
    std::cout << std::setw(line_width) << l << nl;

    std::cout << root();
}
//---------------------------------------------------------------------------------------------------------------------------
void FileSystem::changeDirUp(Line const& line) {
    std::string const& l {line.line()};
    std::cout << std::setw(func_width) << __func__ << tab << '(' << stackDirName() << ')' << tab;

    mDirStack.pop_back();
    std::cout << "Chg: " << stackDirName();
    std::cout << sp << std::setw(line_width) << l << nl;
}
//---------------------------------------------------------------------------------------------------------------------------
void FileSystem::changeDir(Line const& line) {
    std::string const& l {line.line()};
    std::string new_dir {l.substr(5)};
    std::cout << std::setw(func_width) << __func__ << tab << '(' << stackDirName() << ')' << tab;

    auto& sub_dirs = stackSubDirs();
    DirPtr dir_ptr {sub_dirs.find(new_dir)};
    mDirStack.emplace_back(dir_ptr);

    std::cout << "Chg: " << stackDirName();
    std::cout << std::setw(line_width) << l << nl;
}
//---------------------------------------------------------------------------------------------------------------------------
void FileSystem::changeDirHome(Line const& line) {
    std::string const& l {line.line()};
    std::cout << std::setw(func_width) << __func__ << tab << '(' << stackDirName() << ')' << tab;

    mDirStack = {mRoot.begin()};

    std::cout << "Chg: " << stackDirName();
    std::cout << std::setw(line_width) << l << nl;
}
//---------------------------------------------------------------------------------------------------------------------------
FileSystem::DirSubDirs const& FileSystem::root() const {
    return mRoot;
}
