/***************************************************************************************************************************
 * Copyright (c) 2022. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute mIt and/or modify mIt under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that mIt will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#pragma once

//---------------------------------------------------------------------------------------------------------------------------
struct RawPosition {
    int x;
    int y;

    RawPosition operator+=(RawPosition const rhs);
    RawPosition operator-=(RawPosition const rhs);

    RawPosition abs() const;

    friend auto operator<=>(const RawPosition&, const RawPosition&) = default;
    friend RawPosition abs(RawPosition const pos);
    friend std::ostream& operator<<(std::ostream& os, RawPosition p);
};
//---------------------------------------------------------------------------------------------------------------------------
RawPosition RawPosition::operator+=(RawPosition const rhs) {
    return {x += rhs.x, y += rhs.y};
}
//---------------------------------------------------------------------------------------------------------------------------
RawPosition abs(RawPosition const pos) {
    return RawPosition {abs(pos.x), abs(pos.y)};
}
//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, RawPosition p) {
    os << p.x << ' ' << p.y;
    return os;
}
//---------------------------------------------------------------------------------------------------------------------------
RawPosition RawPosition::operator-=(RawPosition const rhs) {
    return {x -= rhs.x, y -= rhs.y};
}
//---------------------------------------------------------------------------------------------------------------------------
RawPosition operator+(RawPosition const lhs, RawPosition const rhs) {
    RawPosition res {lhs};
    return res += rhs;
}
//---------------------------------------------------------------------------------------------------------------------------
RawPosition operator-(RawPosition const lhs, RawPosition const rhs) {
    RawPosition res {lhs};
    return res -= rhs;
}
//---------------------------------------------------------------------------------------------------------------------------
RawPosition RawPosition::abs() const {
//    RawPosition res
    return {::abs(x), ::abs(y)};
}
