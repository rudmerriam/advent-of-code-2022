/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#pragma once

#include <complex>

struct ComplexPosition : std::complex<int> {
    int& x = real();
    int& y = imag();
};

//---------------------------------------------------------------------------------------------------------------------------
// won't work because not a friend
//std::strong_ordering operator<=>(ComplexPosition const& lhs, ComplexPosition const& rhs)  = default;

std::strong_ordering operator<=>(ComplexPosition const& lhs, ComplexPosition const& rhs) {
    return lhs <=> rhs;
}
////---------------------------------------------------------------------------------------------------------------------------
//ComplexPosition abs(ComplexPosition const& lhs) {
//    return {::abs(lhs.real()), ::abs(lhs.imag())};
//}
