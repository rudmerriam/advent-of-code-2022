/***************************************************************************************************************************
 * Copyright (c) 2023. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#include <vector>
#include <ranges>
#include <algorithm>
#include <unordered_set>
#include <iostream>
#include <cassert>
#include <string_view>
#include <fstream>
#include <iomanip>

//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
void print(T& c, char const *delimiter = "\n") {
    for (auto const& e: c) {
        std::cout << e << delimiter;
    }
}
namespace day09::bridge {

    enum class Direction {
        Right, Left, Up, Down
    };

    struct Order {
        Direction direction;
        uint32_t count;

        friend std::istream& operator>>(std::istream& s, Order& order) {
            char dir;
            if (s >> dir >> order.count) {
                switch (dir) {
                    case 'D':
                        order.direction = Direction::Down;
                        break;
                    case 'U':
                        order.direction = Direction::Up;
                        break;
                    case 'L':
                        order.direction = Direction::Left;
                        break;
                    case 'R':
                        order.direction = Direction::Right;
                        break;
                    default:
                        throw std::runtime_error("Can't parse Order, wrong direction.");
                }
            }
            return s;
        }
    };

    struct Position {
        int row;
        int column;
        friend auto operator<=>(const Position&, const Position&) = default;

        friend std::ostream& operator<<(std::ostream& os, Position p) {
            os << p.column << ' ' << p.row;
            return os;
        }
    };
}

template<>
struct std::hash<day09::bridge::Position> {
    size_t operator()(const day09::bridge::Position& pos) const {
        return std::hash<int> { }(pos.row) ^ (std::hash<int> { }(pos.column) << 1);
    }
};

namespace day09::bridge {

    void single_step(Position& head, Direction direction) {
        switch (direction) {
            case Direction::Down:
                head.row--;
                break;
            case Direction::Up:
                head.row++;
                break;
            case Direction::Left:
                head.column--;
                break;
            case Direction::Right:
                head.column++;
                break;
        }
    }

    void follow_step(Position& head, Position& tail) {
        if (std::abs(head.row - tail.row) >= 2) {
            tail.row += (head.row - tail.row) / 2;

            if (std::abs(head.column - tail.column) >= 2) {
                tail.column += (head.column - tail.column) / 2;
            } else {
                tail.column += head.column - tail.column;
            }

        } else if (std::abs(head.column - tail.column) >= 2) {
            tail.column += (head.column - tail.column) / 2;
            tail.row += head.row - tail.row;
        }
    }

    size_t simulate_motions(const std::vector<Order>& orders) {
        std::cout << "\n=============== \n" << __FUNCTION__ << '\n';

        Position head {0, 0}, tail {0, 0};
        std::unordered_set<Position> visited {{0, 0}};

        auto single_order = [&](const Order& order) {

            for ([[maybe_unused]] int c: std::views::iota(0u, order.count)) {
                single_step(head, order.direction);
                follow_step(head, tail);
                visited.insert(tail);
            }
        };
        std::ranges::for_each(orders, single_order);
        std::cout << "=============== \n" << __FUNCTION__ << '\n';
        print(visited);
        std::cout << "=============== \n" << __FUNCTION__ << '\n';
        return visited.size();
    }

    size_t simulate_long_motions(const std::vector<Order>& orders) {
        std::vector<Position> rope(10, {0, 0});
        std::unordered_set<Position> visited {{0, 0}};
        auto single_order = [&](const Order& order) {
// X           for (auto c: std::views::iota(0uz, order.count)) {
            for ([[maybe_unused]] auto c: std::views::iota(0u, order.count)) {
                single_step(rope[0], order.direction);
                // in C++23 we could use std::views::adjacent_view, for now, let's stick with indices
// X                    for (auto idx: std::views::iota(1uz, rope.size()))
                for (auto idx: std::views::iota(1u, rope.size())) {
                    follow_step(rope[idx - 1], rope[idx]);
                }
                visited.insert(rope[9]);
            }
        };
        std::ranges::for_each(orders, single_order);
        return visited.size();
    }
}
//===========================================================================================================================

int test() {
    using namespace day09::bridge;
    std::vector<Order> data {{Direction::Right, 4},
                             {Direction::Up,    4},
                             {Direction::Left,  3},
                             {Direction::Down,  1},
                             {Direction::Right, 4},
                             {Direction::Down,  1},
                             {Direction::Left,  5},
                             {Direction::Right, 2},};

    std::cout << __FUNCTION__ << '\n';
//    assert(simulate_motions(data) == 13);
    simulate_motions(data);
//    assert(simulate_long_motions(data) == 1);

    std::vector<Order> larger {{Direction::Right, 5},
                               {Direction::Up,    8},
                               {Direction::Left,  8},
                               {Direction::Down,  3},
                               {Direction::Right, 17},
                               {Direction::Down,  10},
                               {Direction::Left,  25},
                               {Direction::Up,    20},};

//    assert(simulate_long_motions(larger) == 36);

    return 0;
}

int parse_and_run(std::string path) {
    using namespace day09::bridge;

    std::fstream file(path);
    if (!file.is_open()) {
        std::cerr << "Failed to open " << std::quoted(path) << "\n";
        return 1;
    }

    std::vector<Order> data;
    std::ranges::copy(std::views::istream<Order>(file), std::back_inserter(data));

    std::cout << "The tail has visited " << simulate_motions(data) << " positions.\n";
    std::cout << "When simulating a longer rope, the tail has visited " << simulate_long_motions(data) << " positions.\n";

    return 0;
}

int main() {
    std::cout << test() << '\n';
//    return parse_and_run("../data_9.txt");
    return 0;
}
