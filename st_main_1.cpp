#include <vector>
#include <string>
#include <ranges>
#include <algorithm>
#include <numeric>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <cassert>
#include <fstream>

namespace day01::calories {

    uint64_t max_calories(const std::vector<std::string>& data) {
        auto by_elf = data | std::views::lazy_split(std::string { }) | // group by elf: range{range{string}}
                      std::views::transform(
                          [ ](const auto& elf) -> uint64_t { // sum up the calories for each elf: range{uint64_t}
                              auto to_unsigned = [ ](const auto& in) {
                                  return std::stoull(in);
                              };
                              auto rng = elf | std::views::transform(to_unsigned) |
                                         std::views::common; // range{string} -> range{uint64_t}
                              return std::reduce(rng.begin(), rng.end()); // range{uint64_t} -> uint64_t
                          });
        return std::ranges::max(by_elf);
    }

    uint64_t top_three(const std::vector<std::string>& data) {
        auto by_elf = data | std::views::lazy_split(std::string { }) | // group by elf: range{range{string}}
                      std::views::transform(
                          [ ](const auto& elf) -> uint64_t { // sum up the calories for each elf: range{uint64_t}
                              auto to_unsigned = [ ](const auto& in) {
                                  return std::stoull(in);
                              };
                              auto rng = elf | std::views::transform(to_unsigned) |
                                         std::views::common; // range{string} -> range{uint64_t}
                              return std::reduce(rng.begin(), rng.end()); // range{uint64_t} -> uint64_t
                          });
        std::vector<uint64_t> top(3);
        std::ranges::partial_sort_copy(by_elf, top, std::greater<> { });
        return std::reduce(top.begin(), top.end());
    }

}
int test() {
    std::vector<std::string> test_data {"1000", "2000", "3000", "", "4000", "", "5000", "6000", "", "7000", "8000", "9000",
                                        "", "10000"};
    assert(day01::calories::max_calories(test_data) == 24000);

    return 0;
}

int parse_and_run(std::string path) {
    std::vector<std::string> data;
    std::fstream file(path);
    if (!file.is_open()) {
        std::cerr << "Failed to open " << std::quoted(path) << "\n";
        return 1;
    }
    std::string line;
    while (std::getline(file, line)) {
        data.push_back(line);
    }
    std::cout << "The elf with the maximum number of calorie is carrying " << day01::calories::max_calories(data)
              << " calories worth of food.\n";
    std::cout << "The top 3 elfs are carrying " << day01::calories::top_three(data) << " calories worth of food.\n";
    return 0;
}

int main() {

    return parse_and_run("../data_1.txt");

}
