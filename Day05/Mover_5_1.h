/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include "../AdventCpp.h"

//---------------------------------------------------------------------------------------------------------------------------
struct Mover {

    struct Move {
        int16_t move_cnt;
        int16_t from_stack;
        int16_t to_stack;
    };
    using CrateMoves = std::vector<Move>;
    using CrateStack = std::vector<char>;
    using Stacks = std::array<CrateStack, 10>;

    CrateMoves mCrateMoves;
    Stacks mStacks;

    //---------------------------------------------------------------------------------------------------------------------------
    void dumpMoves(CrateMoves crate_moves) {
        std::cout << "\n*** Moves Cnt: " << crate_moves.size() << '\n';
        for (auto& move: crate_moves) {
            std::cout << '\t' << move.move_cnt << '\t' << move.from_stack << '\t' << move.to_stack << '\n';
        }
        std::cout << "***" << '\n';
    }
    //---------------------------------------------------------------------------------------------------------------------------
    void parseMoves(std::istream& data) {
        std::string line;
        while (data.good()) {
            std::string throw_way;
            Move move;
            data >> throw_way >> move.move_cnt >> throw_way >> move.from_stack >> throw_way >> move.to_stack;

            mCrateMoves.push_back(move);

            data.get();     // eat last nl
            data.peek();    // force EOF so !good
        }
        dumpMoves(mCrateMoves);
    }
    //---------------------------------------------------------------------------------------------------------------------------
    void dumpStacks() {
        std::cout << "\n*** Stacks Cnt: " << mStacks.size() << '\n';
        int16_t stack_pos { };

        for (auto& stks: mStacks) {
            std::cout << stack_pos++ << '\t';
            dumpStack(stks);
        }
        std::cout << "***" << '\n';
    }
    //---------------------------------------------------------------------------------------------------------------------------
    void dumpStack(CrateStack const& stks) const {
        for (auto& s: stks) {
            std::cout << s << ' ';
            std::cout << ' ';
        }
        std::cout << '\n';
    }
    //---------------------------------------------------------------------------------------------------------------------------
    void parseStacks(std::istream& data) {

        std::string line;
        for (;;) {
            std::getline(data, line);
            std::cout << line << '\n';

            if (line[1] == '1') {
                break;
            }

            for (std::size_t j {1}, s {1}; j < line.size(); j += 4, ++s) {
                char& ch {line[j]};

                if (ch != ' ') {
//                std::cout << "stK: " << s << ' ' << ch << '\n';
                    auto& stk {mStacks[s]};
                    stk.emplace(stk.begin(), ch);
                }
            }
        }
        std::getline(data, line);
        dumpStacks();
    }
    //---------------------------------------------------------------------------------------------------------------------------
    void moveCrates() {
        std::cout << "\n*** Move Crates: " << '\n';

        dumpStacks();

        for (auto move: mCrateMoves) {
            std::cout << "vvvv" << '\n';
            std::cout << '\t' << move.move_cnt << '\t' << move.from_stack << '\t' << move.to_stack << '\n';

            auto cnt {move.move_cnt};
            auto& from {mStacks[move.from_stack]};
            auto& to {mStacks[move.to_stack]};

            for (int16_t i { }; i < cnt; ++i) {
                to.push_back(from.back());
                from.pop_back();
            }

            dumpStacks();

            std::cout << "^^^^" << '\n';
        }
        std::cout << "***" << '\n';
    }
};
