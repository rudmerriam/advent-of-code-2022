/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <cmath>
#include <iostream>
#include <typeinfo>

#include "position_array.h"
//#include "position_complex.h"
#include "position_pair.h"
#include "position_raw.h"

namespace rng = std::ranges;
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
void print(T& c, char const *delimiter = "\n") {
    for (auto const& e: c) {
        std::cout << e << delimiter;
    }
}
const char nl {'\n'};
const char sp {' '};
const char tab {'\t'};
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
bool validate(T const expected, T const actual, char const *description = "") {
    bool res {actual != expected};
    if (res) {
        std::cout << description << " validation failed.  Expected: " << expected << " Actual: " << actual << nl;
    }
    return res;
}
//===========================================================================================================================
template<typename T>
void operator_tests() {
    T pos1 {1, 2};
    T pos2 {3, 4};
    T pos3 { };

    std::cout << "\nTest type: " << typeid(pos1).name() << nl;

    validate(1, pos1.x, "X access");
    validate(2, pos1.y, "Y access");

    // check pos3 is {0,0}
    validate(T {0, 0}, pos3, "Pos3 0 check");

    pos3.x = 7;
    validate(T {7, 0}, pos3, "Setting X");

    pos3.y = 8;
    validate(T {7, 8}, pos3, "Setting Y");

    T pos4 { };
    pos4 += pos1;
    validate(pos1, pos4, "Addition Equal");
    pos4 -= pos1;
    validate(T {0, 0}, pos4, "Subtraction Equal");

    validate(T {4, 6}, pos1 + pos2, "Addition");
    validate(T {-2, -2}, pos1 - pos2, "Subtraction");

    validate(true, pos1 < pos2, "Less than");
    validate(true, pos1 != pos2, "Not equal");

    validate(T {1, 1}, abs(T {-1, 1}), "Abs function");
}
//===========================================================================================================================
int main() {
    operator_tests<RawPosition>();
    operator_tests<PairPosition>();
//    operator_tests<ComplexPosition>();
    operator_tests<ArrayPosition>();

    return 0;
}
