/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#include <sstream>
#include <iostream>
#include <valarray>
#include <filesystem>
#include <ranges>
#include <vector>

namespace fs = std::filesystem;
//---------------------------------------------------------------------------------------------------------------------------
const char nl {'\n'};
const char sp {' '};
//---------------------------------------------------------------------------------------------------------------------------
struct Move {
    char direction;
    int8_t steps;

    friend std::istream& operator>>(std::istream& is, Move& m) {
        is >> m.direction >> m.steps;
        return is;
    }

    friend std::ostream& operator<<(std::ostream& os, Move m) {
        os << m.direction << sp << m.steps << nl;
        return os;
    }
};
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
void print(T& c) {
    for (auto const& e: c) {
        std::cout << e;
    }
}
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    char const *test_data {"30373\n"
                           "25512\n"
                           "65332\n"
                           "33549\n"
                           "35390\n"};

    std::istringstream data_file(test_data);

    const auto fsize = fs::file_size("../data_1.txt");
    std::cout << fsize << '\n';

    std::string data;
    int16_t line_len { };

    while (data_file.good()) {
        std::string line;
        std::getline(data_file, line);
        line_len = line.size();

        data += line;

        data_file.peek();
    }

    std::cout << line_len << ' ' << data << nl;

    std::valarray<int16_t> v_data(data.size());

    for (int i { }; auto const ch: data) {
        v_data[i] = ch - '0';
        i++;
    }

    std::cout << nl;
    print(v_data);
    std::cout << nl;

    auto const stride {5};
    auto const num_return {v_data.size() / stride};

    // get a column
    std::valarray<int16_t> col = v_data[std::slice(0, num_return, stride)];
    print(col);
    std::cout << nl;

    for (auto it {std::end(col) - 1}; it >= std::begin(col); it--) {
        std::cout << *it;
    }
    std::cout << nl;

    // get a row
    std::valarray<int16_t> row = v_data[std::slice(5, 5, 1)];
    print(row);
    std::cout << nl;

    std::istringstream df(test_data);
    std::ranges::copy(std::ranges::istream_view<char>(df), std::ostream_iterator<char>(std::cout));
    std::cout << nl;

    std::istringstream moves("U 1 L 1 U 2 R 1 U 2");

//    Move mm;
//    moves >> mm;
//    std::cout << mm.direction << sp << mm.steps << nl;

    std::vector<Move> m_vec;

    std::ranges::copy(std::ranges::istream_view<Move>(moves), std::back_inserter(m_vec));

    print(m_vec);
    std::cout << nl;

    print(m_vec);
    std::cout << nl;

    std::vector<char> c_vec {'a', 'b', 'c'};
    print(c_vec);
    std::cout << nl;

    auto x = std::views::all(c_vec) | std::views::transform([ ](char ch) {
        return char(ch += 1);
    });
    print(x);

//      30373 25512 65332 33549 35390
    return 0;
}
