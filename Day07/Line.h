/***************************************************************************************************************************
 * Copyright (c) 2023. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

//
// Created by rmerriam on 1/1/23.
//

#pragma once

#include "LineTypes.h"

//---------------------------------------------------------------------------------------------------------------------------
struct Line {
    Line() = default;

    explicit Line(std::string& line) : mLine {line} {
        if (line.starts_with("$ cd /")) {
            mLinesType.emplace<ChangeDirHome>();
        } else if (line.starts_with("$ cd ..")) {
            mLinesType.emplace<ChangeDirUp>();
        } else if (line.starts_with("$ cd ")) {
            mLinesType.emplace<ChangeDir>();
        } else if (line.starts_with("$ ls")) {
            mLinesType.emplace<ListFiles>();
        } else if (line.starts_with("dir ")) {
            mLinesType.emplace<SubDirName>();
        } else {
            mLinesType.emplace<FileSize>();
        }
    }
    Line(Line& l) = default;
    Line(Line&& l) = default;

    Line& operator=(Line const&) = default;
    Line& operator=(Line&&) = default;

    [[nodiscard]] std::string line() const {
        return mLine;
    }
    linesVariant mLinesType {FileSize { }};

    friend std::istream& operator>>(std::istream& is, Line& line);
    friend std::ostream& operator<<(std::ostream& os, Line const& line);

private:
    std::string mLine;
};
//---------------------------------------------------------------------------------------------------------------------------
std::istream& operator>>(std::istream& is, Line& line) {
    std::string s;
    std::getline(is, s);

    Line l {s};
    line = l;

    return is;
}
//---------------------------------------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, Line const& line) {
    std::visit([&line](auto&& arg) { std::cout << arg.mName << tab << line.mLine; }, line.mLinesType);
    return os;
}
