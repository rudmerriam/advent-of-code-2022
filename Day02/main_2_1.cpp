/***************************************************************************************************************************
 * Copyright (c) 2023. Mystic Lake Software                                                                                *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/

#include <fstream>
#include <iostream>
#include <array>
#include <sstream>

//---------------------------------------------------------------------------------------------------------------------------
int64_t get_calories(std::ifstream& data) {
    int64_t calories;
    data >> calories;
    data.get();     // eat nl
    std::cout << calories << '\t';
    return calories;
}
//---------------------------------------------------------------------------------------------------------------------------
void open_data(std::ifstream& data) {
    if (!data.is_open()) {
        std::cout << "file not open" << '\n';
        exit(-2);
    }
}
//---------------------------------------------------------------------------------------------------------------------------
int16_t advent_two_one(std::istream& data) {
    std::array<char, 3> plays {'R', 'P', 'S'};
    using score_row = std::array<int, 3>;
    std::array<score_row, 3> score_array {4, 8, 3, 1, 5, 9, 7, 2, 6};

    int16_t score { };

    while (data) {

        char opponent;
        char strategy;

        data >> opponent >> strategy;

        if (data) {
            opponent -= 'A';
            strategy -= 'X';

            std::cout << char(opponent + 'A') << plays[opponent] << ' ' << char(strategy + 'X') << '\n';

            score += score_array[opponent][strategy];

            std::cout << char(opponent + 'A') << plays[opponent] << ' ' << plays[strategy] << ' ';
            std::cout << score_array[opponent][strategy] << '\t' << score << '\n';
        }
    }
    return score;
}
//---------------------------------------------------------------------------------------------------------------------------
std::istringstream test_data("A Y\n B X\n C Z\n");
//---------------------------------------------------------------------------------------------------------------------------
// 13924
int main() {

    int16_t test_score {advent_two_one(test_data)};
    if (test_score != 15) {
        std::cout << "Validation failed. Should be 15 but was " << test_score << '\n';
        exit(-1);
    }

    std::ifstream data("../Day02/data_2.txt");
    open_data(data);

    advent_two_one(data);

    return 0;
}
