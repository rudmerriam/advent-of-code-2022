/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <algorithm>
#include <iostream>

#include "../AdventCpp.h"
#include "Mover_9_1.h"

//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day09/data_9.txt"};

#if 1
        char const *test_data {"R 4\n"
                               "U 4\n"
                               "L 3\n"
                               "D 1\n"
                               "R 4\n"
                               "D 1\n"
                               "L 5\n"
                               "R 2\n"};
#else
        char const *larger {"R 5\n"
                            "U 8\n"
//                            "L 8\n"
//                            "D 3\n"
//                            "R 17\n"
//                            "D 10\n"
//                            "L 25\n"
//                            "U 20\n"
        };
#endif

        Mover mover;
        AdventCpp run(data_file, mover, 13, test_data);
//        AdventCpp run(data_file, mover, 36, larger);

        run.validate();
        std::cout << "Final result: " << run.execute() << '\n';
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }
    return 0;
}
