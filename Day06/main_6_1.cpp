/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <iostream>
#include <algorithm>
#include <set>
#include "../AdventCpp.h"

//---------------------------------------------------------------------------------------------------------------------------
execFunc execute = [ ](std::istream& data) {
    int16_t result { };

    std::string str;
    str.reserve(4);

    std::getline(data, str);
    std::cout << str << '\n';

    for (std::size_t i { }; i < str.size() - 3; ++i) {
        std::set sv {str.begin() + i, str.begin() + i + 4};

        if (sv.size() == 4) {
            std::cout << sv.size() << '\t' << i + 4 << '\n';
            result = i + 4;
            break;
        }
    }

    return result;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    try {
        char const *data_file {"../Day06/data_6.txt"};
        {
            char const *test_data {"mjqjpqmgbljsphdztnvjfqwrcgsmlb"};
            AdventCpp run(data_file, execute, 7, test_data);
            run.validate();
//            auto res {run.execute()};
        }

#if 1
        {
            char const *test_data {"bvwbjplbgvbhsrlpgdmjqwftvncz"};
            AdventCpp run(data_file, execute, 5, test_data);
            run.validate();
        }
        {
            char const *test_data {"nppdvjthqldpwncqszvftbrmjlhg"};
            AdventCpp run(data_file, execute, 6, test_data);
            run.validate();
        }
        {
            char const *test_data {"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"};
            AdventCpp run(data_file, execute, 10, test_data);
            run.validate();
        }
        {
            char const *test_data {"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"};
            AdventCpp run(data_file, execute, 11, test_data);
            run.validate();

            auto res {run.execute()};
            std::cout << "Final result: " << res << '\n';
        }

#endif
    } catch (std::exception& msg) {
        std::cout << msg.what();
        exit(-1);
    }

    return 0;
}
