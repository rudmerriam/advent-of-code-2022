/***************************************************************************************************************************
 * Copyright (c) 2022-2023. Mystic Lake Software                                                                           *
 *                                                                                                                         *
 * This is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License       *
 * as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.   *
 *                                                                                                                         *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.              *
 *                                                                                                                         *
 * You should have received a copy of the GNU General Public License along with this program.                              *
 * If not, see <http:www.gnu.org/licenses/>.                                                                               *
 ***************************************************************************************************************************/
#include <algorithm>
#include <compare>
#include <complex>
#include <iostream>
#include <istream>
#include <ranges>
#include <set>
#include <vector>

namespace rng = std::ranges;
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
void print(T& c, char const *delimiter = "\n") {
    for (auto const& e: c) {
        std::cout << e << delimiter;
    }
}
const char nl {'\n'};
const char sp {' '};
const char tab {'\t'};
//---------------------------------------------------------------------------------------------------------------------------
struct Move {
    char direction;
    int steps;

    friend std::istream& operator>>(std::istream& is, Move& m) {
        is >> m.direction >> m.steps;
        return is;
    }

    friend std::ostream& operator<<(std::ostream& os, Move m) {
        os << m.direction << sp << m.steps;
        return os;
    }
};
//---------------------------------------------------------------------------------------------------------------------------
template<typename T>
auto sign(T a) -> T {
    return a > 0 ? 1 : -1;
}
//---------------------------------------------------------------------------------------------------------------------------
struct Position {
    int x;
    int y;

    friend auto operator<=>(const Position&, const Position&) = default;

    Position operator+=(Position const rhs) {
        return {x += rhs.x, y += rhs.y};
    }

    Position operator-=(Position const rhs) {
        return {x -= rhs.x, y -= rhs.y};
    }

    friend std::ostream& operator<<(std::ostream& os, Position p) {
        os << p.x << sp << p.y;
        return os;
    }

    friend Position abs(Position const pos) {
        return Position {abs(pos.x), abs(pos.y)};
    }
};
Position operator-(Position const lhs, Position const rhs) {
    Position res {lhs};
    return res -= rhs;
}
//---------------------------------------------------------------------------------------------------------------------------
struct Mover {

    //---------------------------------------------------------------------------------------------------------------------------
    void move(std::vector<Move> m_vec) {
        mLead.push_back(mHead);
        mTrail.insert(mTail);

        rng::for_each(m_vec, [this](Move move) {
            std::cout << "--------------\n" << move << sp << tab << mHead << sp << tab << mTail << nl;

            for (int i { }; i < move.steps; ++i) {
                move_head(move.direction);
                moveTail();
                std::cout << tab << tab << mHead << sp << tab << mTail << nl;
            }
        });
    }
    //---------------------------------------------------------------------------------------------------------------------------
    void move_head(char direction) {
        Position col_move {0, 1};
        Position row_move {1, 0};

        switch (direction) {
            case 'U':
                mHead += col_move;
                break;
            case 'D':
                mHead -= col_move;
                break;

            case 'R':
                mHead += row_move;
                break;
            case 'L':
                mHead -= row_move;
                break;
        }
        mLead.push_back(mHead);
    }
    //---------------------------------------------------------------------------------------------------------------------------
    void moveTail() {
        auto diff {mHead - mTail};
        std::cout << "Diff: " << diff << nl;

        auto total_distance {abs(diff.x) + abs(diff.y)};

        if (total_distance >= 3) {
            mTail += Position {sign(diff.x), sign(diff.y)};
            mTrail.insert(mTail);
            std::cout << "diag: " << tab << mHead << sp << tab << mTail << nl;
        }
        if (abs(diff.y) > 1) {
            mTail += Position {0, sign(diff.y)};
            mTrail.insert(mTail);
        }
        if (abs(diff.x) > 1) {
            mTail += Position {sign(diff.x), 0};
            mTrail.insert(mTail);
        }
    }
    //---------------------------------------------------------------------------------------------------------------------------
    std::set<Position> const& trail() const {
        return mTrail;
    }
    //---------------------------------------------------------------------------------------------------------------------------
    std::vector<Position> const& lead() const {
        return mLead;
    }

    Position mHead { };
    Position mTail { };
    std::vector<Position> mLead;
    std::set<Position> mTrail;
};
//---------------------------------------------------------------------------------------------------------------------------
int main() {
    std::istringstream moves("R 4\n"
                             "U 4\n"
                             "L 3\n"
                             "D 1\n"
                             "R 4\n"
                             "D 1\n"
                             "L 5\n"
                             "R 2\n");
    std::vector<Move> m_vec;

    rng::copy(rng::istream_view<Move>(moves), std::back_inserter(m_vec));

    print(m_vec);
    std::cout << nl;

    Mover mover;
    mover.move(m_vec);

    std::cout << nl;
    print(mover.lead(), " \t");

    std::cout << nl << nl;
    print(mover.trail(), " \t");

    return 0;
}
/*
 *
 * 0 0 	1 0 	1 2 	2 0 	2 2 	2 3 	3 0 	3 2 	3 3 	4 1 	4 2 	4 3
 *
 * 0 0 	1 0 	1 2 	2 0 	2 2 	2 4 	3 0 	3 2 	3 3 	3 4 	4 1 	4 2 	4 3
 *
 * 0 0  1 0     1 2     2 0     2 2     2 4     3 0     3 2     3 3     3 4     4 1     4 2     4 3

 */
